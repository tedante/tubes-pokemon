import java.util.*;
import java.io.*;

class Character {
  String name;
  ArrayList<Pokemon> myPokemon = new ArrayList<>();
  int money;
  ArrayList<Item> myBag = new ArrayList<>();

  // Character(String mName) {
  //   name = mName;
  // }

  public void catchPokemon(Pokemon mPokemon) {
    myPokemon.add(mPokemon);
  }

	public void defeatPokemon(Pokemon mPokemon) {
		myPokemon.remove(mPokemon);
	}

}
